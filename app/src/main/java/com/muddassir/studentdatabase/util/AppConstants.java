package com.muddassir.studentdatabase.util;

/**
 * Created by Muddassir on 2/4/2015.
 */
public interface AppConstants {

    String USERNAME_KEY = "username";
    String PASSWORD_DEFAULT = "*******";
    String USERNAME_DEFAULT = "INVALID";
    String PASSWORD_KEY = "password";
    String INVALID_USERNAME_MESSAGE = "Invalid username! Please enter CORRECT credentials.";
    String INVALID_PASSWORD_MESSAGE = "Invalid password! Please enter CORRECT credentials.";
    String INVALID_EMAIL_MESSAGE = "Invalid email! Please enter CORRECT email id.";
    String BLANK_FIELD_MESSAGE = "None of the field can be left blank.";
    String SIGN_UP = "Sign Up";
    String LOGIN = "Login";

    String NAME_LABEL = "Name : ";
    String ROLL_LABEL = "Roll : ";

    int SORT_BY_NAME = 1;
    int SORT_BY_ROLL = 2;

    String UNSELECTED_BACKGROUND_COLOR = "#141452";
    String WHITE_COLOR = "#FFFFFF";
    String BLACK_COLOR = "#000000";

    String[] DROP_ITEMS = {"Choose an Option", "Name", "Roll Number"};

    String FAILED_OPERATION_MESSAGE = "Failed to insert this entry! Try another. Make sure " +
            "duplicate roll number is not inserted.";
    String NO_ENTRY_FOUND_MESSAGE = "No Entry found in the database.";
    String ADDED_SUCCESS_MESSAGE = "Successfully Added Entry.";
    String UPDATE_SUCCESS_MESSAGE = "Successfully Updated Entry.";
    String DELETE_SUCCESS_MESSAGE = "Successfully Deleted Entry.";

    int ADD_REQUEST_CODE = 100;
    int EDIT_REQUEST_CODE = 200;

    int FAILED_OPERATION = -1;
    int ADDED_SUCCESS = 1;
    int UPDATE_SUCCESS = 2;
    int DELETE_SUCCESS = 3;
    int EXTRACTION_SUCCESS = 4;
    int VIEW_SUCCESS = 5;

    int INITIAL_VALUE = 1;
    int FINAL_VALUE = 101;
    long SLEEP_TIME = 10;

    String UPDATE_KEY = "Update";
    String INSERT_KEY = "Insert";
    String DELETE_KEY = "Delete";
    String VIEW_ALL_KEY = "View All";
    String VIEW_KEY = "View Single";
    int OPERATION_KEY = 1;
    int STUDENT_KEY = 0;

    String DIALOG_TITLE = "Choose an Option";
    String DIALOG_MESSAGE = "Completed";
    String ALERT_TITLE = "Sign Up Successful";
    String ALERT_MESSAGE = "Now Please Login to continue.";
    String INAPPROPRIATE_ROLL_MESSAGE = "Roll Number cannot be left blank or can be ZERO!";
    String INAPPROPRIATE_NAME_MESSAGE = "Name cannot Start with Space or Left Blank!";
    String CANCELLED_OPERATION = "Cancelled Operation!";

    String POSITION_KEY = "position";
    String CLICK_KEY = "click";
    String OBJECT_KEY = "student";

    String EDIT_BUTTON = "Edit";
    String VIEW_BUTTON = "View";
}
