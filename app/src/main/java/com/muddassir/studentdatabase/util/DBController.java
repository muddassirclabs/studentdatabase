package com.muddassir.studentdatabase.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.muddassir.studentdatabase.entities.Student;

import java.util.ArrayList;
import java.util.List;

public class DBController implements DBConstants {
    DBHelper dbHelper;
    SQLiteDatabase database;
    Context context;

    public DBController(Context context) {
        this.context = context;
    }

    // This method will open connection to the database
    public void open() {
        dbHelper = new DBHelper(context, DB_NAME, null, DB_VERSION);
        database = dbHelper.getWritableDatabase();
    }

    // This method will close connection to the database
    public void close() {
        database.close();
    }

    // This method will insert the data in the database
    public long insertStudent(Student student) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ROLL_COLUMN, student.getRollNo());
        contentValues.put(NAME_COLUMN, student.getName());
        contentValues.put(EMAIL_COLUMN, student.getEmail());
        contentValues.put(DEPARTMENT_COLUMN, student.getDepartment());
        contentValues.put(ADDRESS_COLUMN, student.getAddress());
        return database.insert(TABLE_NAME, null, contentValues);
    }

    // This method will update the data in the database
    public int updateStudent(Student student) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME_COLUMN, student.getName());
        contentValues.put(EMAIL_COLUMN, student.getEmail());
        contentValues.put(DEPARTMENT_COLUMN, student.getDepartment());
        contentValues.put(ADDRESS_COLUMN, student.getAddress());
        return database.update(TABLE_NAME, contentValues, ROLL_COLUMN + "=?",
                new String[]{Integer.toString(student.getRollNo())});
    }

    // This method will delete the data from the database
    public int deleteStudent(Student student) {
        return database.delete(TABLE_NAME, ROLL_COLUMN + "=?",
                new String[]{Integer.toString(student.getRollNo())});
    }

    // This method will extract all data from the database
    public List<Student> viewAllStudents() {
        List<Student> allStudents = new ArrayList<>();
        int roll;
        String name, email, department, address;
        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                roll = cursor.getInt(ROLL_INDEX);
                name = cursor.getString(NAME_INDEX);
                email = cursor.getString(EMAIL_INDEX);
                department = cursor.getString(DEPARTMENT_INDEX);
                address = cursor.getString(ADDRESS_INDEX);
                allStudents.add(new Student(roll, name, email, department, address));
                cursor.moveToNext();
            }
        }
        return allStudents;
    }

    // This method will extract single row from database
    public Student viewSingleStudents(int rollNumber) {
        Student tempStudent = null;
        int roll;
        String name, email, department, address;
        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME +
                " WHERE " + ROLL_COLUMN + "= " + rollNumber, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                roll = cursor.getInt(ROLL_INDEX);
                name = cursor.getString(NAME_INDEX);
                email = cursor.getString(EMAIL_INDEX);
                department = cursor.getString(DEPARTMENT_INDEX);
                address = cursor.getString(ADDRESS_INDEX);
                tempStudent = new Student(roll, name, email, department, address);
            }
        }
        return tempStudent;
    }
}
