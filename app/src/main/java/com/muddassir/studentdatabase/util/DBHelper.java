package com.muddassir.studentdatabase.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper implements DBConstants {


    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    // This method will create table in the database
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (" +
                        ROLL_COLUMN + " INTEGER PRIMARY KEY NOT NULL, " +
                        NAME_COLUMN + " TEXT NOT NULL, " +
                        EMAIL_COLUMN + " TEXT NOT NULL, " +
                        DEPARTMENT_COLUMN + " TEXT NOT NULL, " +
                        ADDRESS_COLUMN + " TEXT NOT NULL);"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
