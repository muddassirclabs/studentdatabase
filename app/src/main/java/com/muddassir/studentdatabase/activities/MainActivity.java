package com.muddassir.studentdatabase.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.muddassir.studentdatabase.R;
import com.muddassir.studentdatabase.adapters.MainAdapter;
import com.muddassir.studentdatabase.entities.Student;
import com.muddassir.studentdatabase.util.DBController;

import java.util.Collections;
import java.util.Comparator;

import static com.muddassir.studentdatabase.util.AppConstants.ADD_REQUEST_CODE;
import static com.muddassir.studentdatabase.util.AppConstants.BLACK_COLOR;
import static com.muddassir.studentdatabase.util.AppConstants.CLICK_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.DELETE_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.DELETE_SUCCESS;
import static com.muddassir.studentdatabase.util.AppConstants.DELETE_SUCCESS_MESSAGE;
import static com.muddassir.studentdatabase.util.AppConstants.DIALOG_MESSAGE;
import static com.muddassir.studentdatabase.util.AppConstants.DIALOG_TITLE;
import static com.muddassir.studentdatabase.util.AppConstants.DROP_ITEMS;
import static com.muddassir.studentdatabase.util.AppConstants.EDIT_BUTTON;
import static com.muddassir.studentdatabase.util.AppConstants.EDIT_REQUEST_CODE;
import static com.muddassir.studentdatabase.util.AppConstants.EXTRACTION_SUCCESS;
import static com.muddassir.studentdatabase.util.AppConstants.FAILED_OPERATION;
import static com.muddassir.studentdatabase.util.AppConstants.FINAL_VALUE;
import static com.muddassir.studentdatabase.util.AppConstants.INITIAL_VALUE;
import static com.muddassir.studentdatabase.util.AppConstants.NO_ENTRY_FOUND_MESSAGE;
import static com.muddassir.studentdatabase.util.AppConstants.OBJECT_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.OPERATION_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.POSITION_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.SLEEP_TIME;
import static com.muddassir.studentdatabase.util.AppConstants.SORT_BY_NAME;
import static com.muddassir.studentdatabase.util.AppConstants.SORT_BY_ROLL;
import static com.muddassir.studentdatabase.util.AppConstants.STUDENT_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.UNSELECTED_BACKGROUND_COLOR;
import static com.muddassir.studentdatabase.util.AppConstants.UPDATE_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.UPDATE_SUCCESS;
import static com.muddassir.studentdatabase.util.AppConstants.VIEW_ALL_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.VIEW_BUTTON;
import static com.muddassir.studentdatabase.util.AppConstants.VIEW_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.VIEW_SUCCESS;
import static com.muddassir.studentdatabase.util.AppConstants.WHITE_COLOR;

public class MainActivity extends Activity {
    static MainAdapter mainAdapter;
    ListView listView;
    GridView gridView;
    Button viewButton, editButton, deleteButton, listButton, gridButton;
    Spinner dropDown;
    ProgressDialog progressDialog;
    Student studentObject;
    int objectPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);
        gridView = (GridView) findViewById(R.id.gridView);
        listButton = (Button) findViewById(R.id.list_view_b);
        gridButton = (Button) findViewById(R.id.grid_view_b);

        // Set Spinner(Drop Down Menu)
        dropDown = (Spinner) findViewById(R.id.spinner);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, DROP_ITEMS) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;

                // If this is the initial dummy entry, make it hidden
                if (position == 0) {
                    TextView tv = new TextView(getContext());
                    tv.setHeight(0);
                    v = tv;
                } else {
                    v = super.getDropDownView(position, null, parent);
                }
                return v;
            }
        };
        dropDown.setAdapter(arrayAdapter);

        // Set adapters for ListView and GridView
        mainAdapter = new MainAdapter(MainActivity.this);
        listView.setAdapter(mainAdapter);
        gridView.setAdapter(mainAdapter);

        // Call to AsyncTask to update the List View
        new DBInteractions().execute(null, VIEW_ALL_KEY);

        // Setting Item Click Listeners for ListView
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                itemClick(parent, view, position, id);
            }
        });

        // Setting Item Click Listeners for GridView
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                itemClick(parent, view, position, id);
            }
        });

        // Setting Item Select Listeners for Spinner(Drop Down)
        dropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == SORT_BY_ROLL) {
                    sortByRoll();
                    mainAdapter.notifyDataSetChanged();
                } else if (position == SORT_BY_NAME) {
                    sortByName();
                    mainAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_button:
                openAddStudent();
                break;
            case R.id.list_view_b:
                setListView();
                break;
            case R.id.grid_view_b:
                setGridView();
                break;
            case R.id.logout_button:
                logout();
            default:
                break;
        }
    }

    // Logout from the system and re-open the login activity
    private void logout() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    void setListView() {
        listView.setVisibility(View.VISIBLE);
        gridButton.setBackgroundColor(Color.parseColor(UNSELECTED_BACKGROUND_COLOR));
        gridButton.setTextColor(Color.parseColor(WHITE_COLOR));
        listButton.setBackgroundColor(Color.parseColor(WHITE_COLOR));
        listButton.setTextColor(Color.parseColor(BLACK_COLOR));
        gridView.setVisibility(View.INVISIBLE);
    }

    void setGridView() {
        gridView.setVisibility(View.VISIBLE);
        listButton.setBackgroundColor(Color.parseColor(UNSELECTED_BACKGROUND_COLOR));
        listButton.setTextColor(Color.parseColor(WHITE_COLOR));
        gridButton.setBackgroundColor(Color.parseColor(WHITE_COLOR));
        gridButton.setTextColor(Color.parseColor(BLACK_COLOR));
        listView.setVisibility(View.INVISIBLE);
    }

    // It will open the next activity(StudentOperations) to add a new student record
    void openAddStudent() {
        Intent intent = new Intent(this, StudentOperations.class);
        startActivityForResult(intent, ADD_REQUEST_CODE);
    }

    /* This function handle the Button Click Listeners.
    1. View button will open the activity to show the details of the student
    2. Edit button will open the activity to edit the desired details of the student
    3. Delete button will call AsyncTask to remove the entry specified from the database
     */
    public void itemClick(AdapterView<?> parent, View view, final int position, long id) {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.activity_option_dialog);
        dialog.setTitle(DIALOG_TITLE);
        viewButton = (Button) dialog.findViewById(R.id.view);
        editButton = (Button) dialog.findViewById(R.id.edit);
        deleteButton = (Button) dialog.findViewById(R.id.delete);

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DBInteractions().execute(mainAdapter.getItem(position), VIEW_KEY);
                dialog.dismiss();
            }
        });
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objectPosition = position;
                new DBInteractions().execute(mainAdapter.getItem(position), UPDATE_KEY);
                dialog.dismiss();
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DBInteractions().execute(mainAdapter.getData().get(position), DELETE_KEY);
                mainAdapter.getData().remove(position);
                mainAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    // This method will sort the list view by Name
    public void sortByName() {
        Collections.sort(mainAdapter.getData(), new Comparator<Student>() {
            @Override
            public int compare(Student lhs, Student rhs) {
                return (lhs.getName().compareToIgnoreCase(rhs.getName()));
            }
        });
    }

    // This method will sort the list view by Roll Number
    public void sortByRoll() {
        Collections.sort(mainAdapter.getData(), new Comparator<Student>() {
            @Override
            public int compare(Student lhs, Student rhs) {
                return (lhs.getRollNo() - rhs.getRollNo());
            }
        });
    }

    // This method will check for insertion or change of data in the called intent
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();
                mainAdapter.getData().add((Student) bundle.get(OBJECT_KEY));
                mainAdapter.notifyDataSetChanged();
            }
        } else if (requestCode == EDIT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();
                mainAdapter.getData().remove(bundle.getInt(POSITION_KEY));
                mainAdapter.getData().add((Student) bundle.get(OBJECT_KEY));
                mainAdapter.notifyDataSetChanged();
            }
        }
        sortByRoll();
        dropDown.setSelection(0);
    }

    /* This is the sub-class that inherits AsyncTask to perform database related operations in
         background.
         */
    class DBInteractions extends AsyncTask<Object, Integer, Integer> {

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setTitle(DIALOG_TITLE);
            progressDialog.setMessage(DIALOG_MESSAGE);
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.show();

            for (int i = INITIAL_VALUE; i < FINAL_VALUE; i++) {
                try {
                    Thread.sleep(SLEEP_TIME);
                    onProgressUpdate(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            super.onPreExecute();
        }

        @Override
        // This is the actual method that will perform database actions in background
        protected Integer doInBackground(Object[] params) {
            DBController dbController = new DBController(MainActivity.this);
            // For Extracting all values from database
            if (params[OPERATION_KEY].equals(VIEW_ALL_KEY)) {
                dbController.open();
                MainActivity.mainAdapter.setData(dbController.viewAllStudents());
                dbController.close();
                if (MainActivity.mainAdapter.getData().size() > 0) {
                    return EXTRACTION_SUCCESS;
                }
            }
            // For deleting a value from database
            else if (params[OPERATION_KEY].equals(DELETE_KEY)) {
                dbController.open();
                int result = dbController.deleteStudent((Student) params[STUDENT_KEY]);
                dbController.close();
                if (result > 0) {
                    return DELETE_SUCCESS;
                }
            }
            // For Extracting single value from database
            else if (params[OPERATION_KEY].equals(VIEW_KEY)) {
                dbController.open();
                studentObject = (Student) params[STUDENT_KEY];
                int rollNumber = studentObject.getRollNo();
                studentObject = dbController.viewSingleStudents(rollNumber);
                dbController.close();
                if (studentObject.getRollNo() > 0) {
                    return VIEW_SUCCESS;
                }
            }
            // For Extracting single value from database
            else if (params[OPERATION_KEY].equals(UPDATE_KEY)) {
                dbController.open();
                studentObject = (Student) params[STUDENT_KEY];
                int rollNumber = studentObject.getRollNo();
                studentObject = dbController.viewSingleStudents(rollNumber);
                dbController.close();
                if (studentObject.getRollNo() > 0) {
                    return UPDATE_SUCCESS;
                }
            }
            return FAILED_OPERATION;
        }

        @Override
        // This updates the progress as well as progress dialog
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressDialog.setProgress(values[0]);
        }

        @Override
        // This method checks for results of the background action
        protected void onPostExecute(Integer o) {
            progressDialog.dismiss();

            // If the operation performed is failed
            if (o == FAILED_OPERATION) {
                Toast.makeText(MainActivity.this,
                        NO_ENTRY_FOUND_MESSAGE, Toast.LENGTH_SHORT).show();
            }

            // For Extraction is successful
            else if (o == EXTRACTION_SUCCESS) {
                MainActivity.mainAdapter.notifyDataSetChanged();
                sortByRoll();
                dropDown.setSelection(0);
            }
            // For Deletion is successful
            else if (o == DELETE_SUCCESS) {
                Toast.makeText(MainActivity.this,
                        DELETE_SUCCESS_MESSAGE, Toast.LENGTH_SHORT).show();
            }
            // For single Extraction is successful and open view activity(StudentOperations)
            else if (o == VIEW_SUCCESS) {
                Intent intent = new Intent(MainActivity.this, StudentOperations.class);
                intent.putExtra(CLICK_KEY, VIEW_BUTTON);
                intent.putExtra(OBJECT_KEY, studentObject);
                startActivity(intent);
            }
            // For single Extraction is successful and open edit activity(StudentOperations)
            else if (o == UPDATE_SUCCESS) {
                Intent intent = new Intent(MainActivity.this, StudentOperations.class);
                intent.putExtra(CLICK_KEY, EDIT_BUTTON);
                intent.putExtra(POSITION_KEY, objectPosition);
                intent.putExtra(OBJECT_KEY, studentObject);
                startActivityForResult(intent, EDIT_REQUEST_CODE);
            }
        }
    }
}