package com.muddassir.studentdatabase.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.muddassir.studentdatabase.R;

import static com.muddassir.studentdatabase.util.AppConstants.ALERT_MESSAGE;
import static com.muddassir.studentdatabase.util.AppConstants.ALERT_TITLE;
import static com.muddassir.studentdatabase.util.AppConstants.BLANK_FIELD_MESSAGE;
import static com.muddassir.studentdatabase.util.AppConstants.INVALID_PASSWORD_MESSAGE;
import static com.muddassir.studentdatabase.util.AppConstants.INVALID_USERNAME_MESSAGE;
import static com.muddassir.studentdatabase.util.AppConstants.LOGIN;
import static com.muddassir.studentdatabase.util.AppConstants.PASSWORD_DEFAULT;
import static com.muddassir.studentdatabase.util.AppConstants.PASSWORD_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.SIGN_UP;
import static com.muddassir.studentdatabase.util.AppConstants.USERNAME_DEFAULT;
import static com.muddassir.studentdatabase.util.AppConstants.USERNAME_KEY;


public class LoginActivity extends Activity {

    SharedPreferences preferences;
    EditText userName, password;
    Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userName = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        loginButton = (Button) findViewById(R.id.login_btn);

        preferences = PreferenceManager
                .getDefaultSharedPreferences(LoginActivity.this);

        // If no user yet registered set Button text to Sign Up
        if (preferences.getString(USERNAME_KEY, USERNAME_DEFAULT).equals(USERNAME_DEFAULT)) {
            loginButton.setText(SIGN_UP);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_btn:
                userAuthentication();
                break;
            case R.id.cancel_btn:
                finish();
                System.exit(0);
                break;
        }
    }

    private void userAuthentication() {
        String user, pass;
        user = userName.getText().toString();
        pass = password.getText().toString();

        /**
         * Authenticating ADMIN access
         */
        if (user.equals("") || pass.equals("")) {
            Toast.makeText(LoginActivity.this, BLANK_FIELD_MESSAGE, Toast.LENGTH_SHORT).show();
        } else {
            if (preferences.getString(USERNAME_KEY, USERNAME_DEFAULT).equals(user)) {
                if (preferences.getString(PASSWORD_KEY, PASSWORD_DEFAULT).equals(pass)) {
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, INVALID_PASSWORD_MESSAGE, Toast.LENGTH_SHORT).show();
                }
                /**
                 * Creating a new Shared Preference(SignUp) to store username and password if it does not exists
                 * already.
                 */
            } else if (preferences.getString(USERNAME_KEY, USERNAME_DEFAULT).equals(USERNAME_DEFAULT)) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(USERNAME_KEY, user);
                editor.putString(PASSWORD_KEY, pass);
                editor.commit();
                loginButton.setText(LOGIN);

                // This AlertDialog will notify the successful sign up
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle(ALERT_TITLE);
                builder.setMessage(ALERT_MESSAGE);
                builder.setCancelable(true);
                builder.setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                userName.setText("");
                                password.setText("");
                                dialog.cancel();
                            }
                        });
                builder.setNegativeButton(R.string.close,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            } else {
                Toast.makeText(LoginActivity.this, INVALID_USERNAME_MESSAGE, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
