package com.muddassir.studentdatabase.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.muddassir.studentdatabase.R;
import com.muddassir.studentdatabase.entities.Student;
import com.muddassir.studentdatabase.util.DBController;

import java.util.regex.Pattern;

import static com.muddassir.studentdatabase.util.AppConstants.ADDED_SUCCESS;
import static com.muddassir.studentdatabase.util.AppConstants.ADDED_SUCCESS_MESSAGE;
import static com.muddassir.studentdatabase.util.AppConstants.BLANK_FIELD_MESSAGE;
import static com.muddassir.studentdatabase.util.AppConstants.CANCELLED_OPERATION;
import static com.muddassir.studentdatabase.util.AppConstants.CLICK_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.DIALOG_MESSAGE;
import static com.muddassir.studentdatabase.util.AppConstants.DIALOG_TITLE;
import static com.muddassir.studentdatabase.util.AppConstants.EDIT_BUTTON;
import static com.muddassir.studentdatabase.util.AppConstants.FAILED_OPERATION;
import static com.muddassir.studentdatabase.util.AppConstants.FAILED_OPERATION_MESSAGE;
import static com.muddassir.studentdatabase.util.AppConstants.FINAL_VALUE;
import static com.muddassir.studentdatabase.util.AppConstants.INAPPROPRIATE_NAME_MESSAGE;
import static com.muddassir.studentdatabase.util.AppConstants.INAPPROPRIATE_ROLL_MESSAGE;
import static com.muddassir.studentdatabase.util.AppConstants.INITIAL_VALUE;
import static com.muddassir.studentdatabase.util.AppConstants.INSERT_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.INVALID_EMAIL_MESSAGE;
import static com.muddassir.studentdatabase.util.AppConstants.OBJECT_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.OPERATION_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.POSITION_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.SLEEP_TIME;
import static com.muddassir.studentdatabase.util.AppConstants.STUDENT_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.UPDATE_KEY;
import static com.muddassir.studentdatabase.util.AppConstants.UPDATE_SUCCESS;
import static com.muddassir.studentdatabase.util.AppConstants.UPDATE_SUCCESS_MESSAGE;
import static com.muddassir.studentdatabase.util.AppConstants.VIEW_BUTTON;

public class StudentOperations extends Activity {

    static Student newStudent;
    EditText studentName, rollNumber, emailID, department, address;
    TextView title;
    Bundle bundle;
    Student student;
    Button save, cancel;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_operations);
        studentName = (EditText) findViewById(R.id.studentname);
        rollNumber = (EditText) findViewById(R.id.rollnumber);
        emailID = (EditText) findViewById(R.id.studentemail);
        department = (EditText) findViewById(R.id.studentdept);
        address = (EditText) findViewById(R.id.studentaddress);
        save = (Button) findViewById(R.id.save);
        cancel = (Button) findViewById(R.id.cancel);

        title = (TextView) findViewById(R.id.main);

        // Extract the bundle received here and check for content
        bundle = getIntent().getExtras();
        if (bundle != null) {
            // Check if button clicked was 'View Button'
            if (bundle.get(CLICK_KEY).equals(VIEW_BUTTON)) {

                student = (Student) bundle.get(OBJECT_KEY);
                studentName.setText(student.getName());
                rollNumber.setText("" + student.getRollNo());
                emailID.setText(student.getEmail());
                department.setText(student.getDepartment());
                address.setText(student.getAddress());

                title.setText(R.string.view_student_title);
                studentName.setEnabled(false);
                rollNumber.setEnabled(false);
                emailID.setEnabled(false);
                department.setEnabled(false);
                address.setEnabled(false);

                save.setVisibility(View.INVISIBLE);
                cancel.setText(R.string.close);

                // Check if button clicked was 'Edit Button'
            } else if (bundle.get(CLICK_KEY).equals(EDIT_BUTTON)) {

                student = (Student) bundle.get(OBJECT_KEY);
                position = bundle.getInt(POSITION_KEY);
                studentName.setText(student.getName());
                rollNumber.setText("" + student.getRollNo());
                emailID.setText(student.getEmail());
                department.setText(student.getDepartment());
                address.setText(student.getAddress());

                rollNumber.setEnabled(false);
                title.setText(R.string.edit_student_title);
                save.setText(R.string.update);
            }
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save:
                addStudent();
                break;
            case R.id.cancel:
                Toast.makeText(this, CANCELLED_OPERATION, Toast.LENGTH_SHORT).show();
                finish();
            default:
                break;
        }
    }

    /* This method performs the actual activity
    1. It checks for the request type i.e. insert or edit
    2. based on the request it sets the intent to be sent back to the calling activity
    3. then it calls for the AsyncTask to perform database operations
    * */
    private void addStudent() {
        String name = studentName.getText().toString();
        String roll = rollNumber.getText().toString();
        String email = emailID.getText().toString();
        String dept = department.getText().toString();
        String add = address.getText().toString();

        if (name.equals("") || name.indexOf(" ") == 0) {
            Toast.makeText(this, INAPPROPRIATE_NAME_MESSAGE, Toast.LENGTH_SHORT).show();
        } else if (roll.equals("") || Integer.parseInt(roll) == 0) {
            Toast.makeText(this, INAPPROPRIATE_ROLL_MESSAGE, Toast.LENGTH_SHORT).show();
        } else if (!validEmail(email)) {
            Toast.makeText(this, INVALID_EMAIL_MESSAGE, Toast.LENGTH_SHORT).show();
        } else if (dept.equals("") || add.equals("")) {
            Toast.makeText(this, BLANK_FIELD_MESSAGE, Toast.LENGTH_SHORT).show();
        } else {
            // This operation will work if Edit button is clicked
            if (bundle != null && bundle.get(CLICK_KEY).equals(EDIT_BUTTON)) {
                name = name.trim();
                newStudent = new Student(Integer.parseInt(roll), name, email, dept, add);
                new DBInteractions().execute(newStudent, UPDATE_KEY);

                // This operation will work if View button is clicked
            } else {
                name = name.trim();
                newStudent = new Student(Integer.parseInt(roll), name, email, dept, add);
                new DBInteractions().execute(newStudent, INSERT_KEY);
            }
        }
    }

    // This will validate the correct email pattern
    private boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    // This class will help in database interactions in background
    class DBInteractions extends AsyncTask<Object, Integer, Integer> {
        ProgressDialog progressDialog;
        Intent intent = new Intent();

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(StudentOperations.this);
            progressDialog.setTitle(DIALOG_TITLE);
            progressDialog.setMessage(DIALOG_MESSAGE);
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.show();

            for (int i = INITIAL_VALUE; i < FINAL_VALUE; i++) {
                try {
                    Thread.sleep(SLEEP_TIME);
                    onProgressUpdate(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            super.onPreExecute();
        }

        @Override
        // This is the actual method that will perform database actions in background
        protected Integer doInBackground(Object[] params) {
            DBController dbController = new DBController(StudentOperations.this);

            // For inserting a value in the database
            if (params[OPERATION_KEY].equals(INSERT_KEY)) {
                dbController.open();
                Integer result = (int) dbController.insertStudent((Student) params[STUDENT_KEY]);
                dbController.close();
                if (result > 0) {
                    return ADDED_SUCCESS;
                }

                // For updating a value in the database
            } else if (params[OPERATION_KEY].equals(UPDATE_KEY)) {
                dbController.open();
                int result = dbController.updateStudent((Student) params[STUDENT_KEY]);
                dbController.close();
                if (result > 0) {
                    return UPDATE_SUCCESS;
                }
            }
            return FAILED_OPERATION;
        }

        @Override
        // This updates the progress as well as progress dialog
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressDialog.setProgress(values[0]);

        }

        @Override
        // This method checks for results of the background action
        protected void onPostExecute(Integer o) {
            progressDialog.dismiss();

            // If the operation performed is failed
            if (o == FAILED_OPERATION) {
                Toast.makeText(StudentOperations.this,
                        FAILED_OPERATION_MESSAGE, Toast.LENGTH_SHORT).show();
            }

            // For Insertion is successful
            else if (o == ADDED_SUCCESS) {
                intent.putExtra(OBJECT_KEY, newStudent);
                setResult(RESULT_OK, intent);
                Toast.makeText(StudentOperations.this,
                        ADDED_SUCCESS_MESSAGE, Toast.LENGTH_SHORT).show();
                finish();
            }

            // For Update is successful
            else if (o == UPDATE_SUCCESS) {
                intent.putExtra(OBJECT_KEY, newStudent);
                intent.putExtra(POSITION_KEY, position);
                setResult(RESULT_OK, intent);
                Toast.makeText(StudentOperations.this,
                        UPDATE_SUCCESS_MESSAGE, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
}