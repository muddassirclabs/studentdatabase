package com.muddassir.studentdatabase.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.muddassir.studentdatabase.R;
import com.muddassir.studentdatabase.entities.Student;

import java.util.ArrayList;
import java.util.List;

import static com.muddassir.studentdatabase.util.AppConstants.NAME_LABEL;
import static com.muddassir.studentdatabase.util.AppConstants.ROLL_LABEL;

public class MainAdapter extends BaseAdapter {
    List<Student> data = new ArrayList<>();
    Context context;

    public MainAdapter(Context ctx) {
        this.context = ctx;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.list_item, null);

        TextView itemName = (TextView) view.findViewById(R.id.student_name);
        itemName.setText(NAME_LABEL + data.get(position).getName());

        TextView itemRoll = (TextView) view.findViewById(R.id.roll_number);
        itemRoll.setText(ROLL_LABEL + data.get(position).getRollNo());

        return view;
    }

    public List<Student> getData() {
        return data;
    }

    public void setData(List<Student> data) {
        this.data = data;
    }
}

