/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muddassir.studentdatabase.entities;

import java.io.Serializable;


public class Student implements Serializable {

    //Data members for students
    private String name;
    private String address;
    private String department;
    private String email;
    private int rollNo;

    //constructor that will generate roll number as well
    public Student(int roll, String name, String email, String department, String address) {
        this.name = name;
        this.rollNo = roll;
        this.address = address;
        this.department = department;
        this.email = email;
    }

    //Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRollNo() {
        return rollNo;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
